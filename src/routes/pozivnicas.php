<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Firebase\JWT\JWT;

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../../src/config/');
$dotenv->load();

$container = $app->getContainer();
$container["jwt"] = function ($container) {
    return new StdClass;
};

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', "X-Requested-With, Content-Type, Accept, Origin, Authorization")
            ->withHeader('Access-Control-Allow-Methods', ["GET, POST, PUT, DELETE, PATCH, OPTIONS"]);
});

$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => getenv('SECRET_KEY'),
    "rules" => [
        new \Slim\Middleware\JwtAuthentication\RequestPathRule([
            "path" => "/",
            "passthrough" => ["/api/potvrdas", "/api/token-auth", "/api/images", "/api/videos", "/api/ping", "/api/dump", "/api/kreiraj"]
        ]),
        new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
            "passthrough" => ["OPTIONS", "PUT"]
        ])
    ]
]));
/* This is just for debugging, not usefull in real life. */
$app->get("/api/dump", function ($request, $response, $arguments) {
    $test = getId(5);
    return $response->withJson(array('dump' => $test), 200);
});
$app->put("/api/dump", function ($request, $response, $arguments) {
    return $response->withJson(array('dump' => 'put'), 200);
});
$app->get("/api/test", function ($request, $response, $arguments) {
    $token = $request->getAttribute("token");
    return $response->withJson(array('token' => $token), 200);
});
// $app->post("/api/kreiraj", function ($request, $response, $arguments) {
//     $hashPassword = password_hash("password", PASSWORD_BCRYPT);

//     $sqlUpit = "INSERT INTO korisnik VALUES (default, 'ivan', 'Ivan', 'http://primjer.com', :lozinka)";

//     $db = new db();
//     $db = $db->connect();
//     $stmt = $db->prepare($sqlUpit);
//     $stmt->bindParam(":lozinka", $hashPassword);
//     $pozivnica = $stmt->execute();
//     $db = null;

//     return $response->withJson(array('spremljeno' => 'super'), 200);
// });

// JWT Token
$app->post("/api/token-auth", function ($request, $response, $arguments) {
    $podatci = json_decode($request->getBody());
    $korisnickoIme = $podatci->username;
    $loz = $podatci->password;

    $sqlUpit = "SELECT * FROM korisnik WHERE korisnickoIme = :korIme";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sqlUpit);
        $stmt->bindParam(":korIme", $korisnickoIme);
        $stmt->execute();
        $korisnik = $stmt->fetchObject();
        $db = null;

        if ($korisnik) {
            // return $response->withJson(array('korisnik' => $korisnik), 200);
            if (password_verify($loz, $korisnik->lozinka)) {
                $token = stvoriToken($korisnik->korisnickoIme, $korisnik->ime, $korisnik->avatar);
                return $response->withJson(array('token' => $token), 200);
            } else {
                return $response->withJson(array('status' => 'Sifra nije dobra'), 401);
            }

        } else {
            return $response->withJson(array('status' => 'Ne postoji korisnik'), 401);
        }

    } catch(PDOException $e) {
        return $response->withJson(array('error' => $e->getMessage()),422);
    }
});

// GET all pozivnice
$app->get('/api/pozivnicas', function(Request $request, Response $response) {
    $sqlUpit = "SELECT * FROM pozivnicas";
    
    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sqlUpit);
        $pozivnice = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        // $response = $response->withHeader('Content-Type', 'application/json');
        // $response = $response->withStatus(200);
        return $response->withJson(array('pozivnicas' => $pozivnice), 200);

        // echo json_encode($pozivnice);
    } catch(PDOException $e) {
        return $response->withJson(array('error' => $e->getMessage()),422);
    }
});

// GET single pozivnice
$app->get('/api/pozivnicas/{id}', function(Request $request, Response $response) {
    $id = $request->getAttribute("id");

    $sqlUpit = "SELECT * FROM pozivnicas WHERE id = :id";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sqlUpit);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $pozivnica = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($pozivnica) {
            return $response->withJson(array('pozivnicas' => $pozivnica), 200);
        } else {
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
    } catch(PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// ADD pozivnice
$app->post('/api/pozivnicas', function(Request $request, Response $response) {
    $podatci = json_decode($request->getBody());
    $objektPozivnice = $podatci->pozivnica;

    $sqlUpit = "INSERT INTO pozivnicas VALUES(:id, :imePrezime, :brojPozvanihOsoba, :brojPotvrdjenihOsoba, :potvrdjenDolazak, :datumPotvrde)";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sqlUpit);
        $id = getId(5);
        $var = 0;
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":imePrezime", $objektPozivnice->imePrezime);
        $stmt->bindParam(":brojPozvanihOsoba", $objektPozivnice->brojPozvanihOsoba);
        $stmt->bindParam(":brojPotvrdjenihOsoba", $objektPozivnice->brojPotvrdjenihOsoba);
        $stmt->bindParam(":potvrdjenDolazak", $var);
        $stmt->bindParam(":datumPotvrde", $objektPozivnice->datumPotvrde);

        $pozivnica = $stmt->execute();
        $db = null;

        if($pozivnica) {
            $db = new db();
            $db = $db->connect();

            $sqlUpit = "SELECT * FROM pozivnicas WHERE id = :id";
            $stmt = $db->prepare($sqlUpit);
            $stmt->bindParam(":id", $id);
            $stmt->execute();
            $pozivnica = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return $response->withJson(array('pozivnicas' => $pozivnica), 200);
        } else {
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
    } catch(PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// DELETE single pozivnice
$app->delete('/api/pozivnicas/{id}', function(Request $request, Response $response) {
    $id = $request->getAttribute("id");

    $sqlUpit = "DELETE FROM pozivnicas WHERE id = :id";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sqlUpit);
        $stmt->bindParam(":id", $id);
        $pozivnica = $stmt->execute();
        
        $db = null;

        if($pozivnica) {
            return $response->withJson(array(), 200);
        } else {
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
    } catch(PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// UPDATE pozivnice
$app->put('/api/pozivnicas/{id}', function(Request $request, Response $response) {
    $id = $request->getAttribute("id");
    $podatci = json_decode($request->getBody());
    $objektPozivnice = $podatci->pozivnica;

    $sqlUpit = "UPDATE pozivnicas SET 
        imePrezime = :imePrezime, 
        brojPozvanihOsoba = :brojPozvanihOsoba, 
        brojPotvrdjenihOsoba = :brojPotvrdjenihOsoba, 
        potvrdjenDolazak = :potvrdjenDolazak, datumPotvrde = :datumPotvrde WHERE id = :id";

    $boolZaDolazak;
    if ($objektPozivnice->datumPotvrde == true) {
        $boolZaDolazak = 1;
    } else {
        $boolZaDolazak = 0;
    }

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sqlUpit);
       
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":imePrezime", $objektPozivnice->imePrezime);
        $stmt->bindParam(":brojPozvanihOsoba", $objektPozivnice->brojPozvanihOsoba);
        $stmt->bindParam(":brojPotvrdjenihOsoba", $objektPozivnice->brojPotvrdjenihOsoba);
        $stmt->bindParam(":potvrdjenDolazak", $boolZaDolazak);
        $stmt->bindParam(":datumPotvrde", $objektPozivnice->datumPotvrde);

        $pozivnica = $stmt->execute();
        $db = null;

        if($pozivnica) {
            $db = new db();
            $db = $db->connect();

            $sqlUpit = "SELECT * FROM pozivnicas WHERE id = :id";
            $stmt = $db->prepare($sqlUpit);
            $stmt->bindParam(":id", $id);
            $stmt->execute();
            $pozivnica = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return $response->withJson(array('pozivnicas' => $pozivnica), 200);
        } else {
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
    } catch(PDOException $e) {
        return $response->withJson(array('error' => $e->getMessage()),422);
    }
});

// GET single potvrda
$app->get('/api/potvrdas/{id}', function(Request $request, Response $response) {
    $id = $request->getAttribute("id");

    $sqlUpit = "SELECT id, imePrezime, brojPotvrdjenihOsoba, potvrdjenDolazak FROM pozivnicas WHERE id = :id";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sqlUpit);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $pozivnica = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($pozivnica) {
            return $response->withJson(array('potvrdas' => $pozivnica), 200);
        } else {

            return $response->withJson(array(), 404);
        }
    } catch(PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// UPDATE potvrde
$app->put('/api/potvrdas/{id}', function(Request $request, Response $response) {
    $id = $request->getAttribute("id");
    $podatci = json_decode($request->getBody());
    $objektPozivnice = $podatci->potvrda;

    $sqlUpit = "UPDATE pozivnicas SET 
        brojPotvrdjenihOsoba = :brojPotvrdjenihOsoba, 
        potvrdjenDolazak = :potvrdjenDolazak, 
        datumPotvrde = :datumPotvrde
        WHERE id = :id";


    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sqlUpit);
       
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":brojPotvrdjenihOsoba", $objektPozivnice->brojPotvrdjenihOsoba);
        $stmt->bindParam(":potvrdjenDolazak", $objektPozivnice->potvrdjenDolazak);
        $trenutnoVrijeme = time();
        $stmt->bindParam(":datumPotvrde", $trenutnoVrijeme);

        $pozivnica = $stmt->execute();
        $db = null;

        if($pozivnica) {
            $db = new db();
            $db = $db->connect();

            $sqlUpit = "SELECT id, imePrezime, brojPotvrdjenihOsoba, potvrdjenDolazak FROM pozivnicas WHERE id = :id";
            $stmt = $db->prepare($sqlUpit);
            $stmt->bindParam(":id", $id);
            $stmt->execute();
            $pozivnica = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return $response->withJson(array('potvrdas' => $pozivnica), 200);
        } else {
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
        
    } catch(PDOException $e) {
        return $response->withJson(array('error' => $e->getMessage()),422);
    }
});

// GET all slike
$app->get('/api/images', function(Request $request, Response $response) {
    $sqlUpit = "SELECT * FROM slike";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sqlUpit);
        $slike = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        return $response->withJson(array('images' => $slike), 200);
    } catch(PDOException $e) {
        return $response->withJson(array('error' => $e->getMessage()),422);
    }
});

// GET single video
$app->get('/api/videos/{id}', function(Request $request, Response $response) {
    $id = $request->getAttribute("id");

    $sqlUpit = "SELECT * FROM youtubevideo WHERE id = :id";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sqlUpit);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $video = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if($video) {
            return $response->withJson(array('videos' => $video), 200);
        } else {
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
    } catch(PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


function getId($length){
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    do {
        $token = substr(str_shuffle($codeAlphabet), 0, $length);
        
        $baza = new db();
        $baza = $baza->connect();
        $sqlUpit = "SELECT id FROM pozivnicas WHERE id = :id";
        $stmt = $baza->prepare($sqlUpit);
        $stmt->bindParam(":id", $token);
        $stmt->execute();
        $pozivnica = $stmt->fetchAll(PDO::FETCH_OBJ);
        $baza = null;
    } while ($pozivnica);

    return $token;
};

function stvoriToken($korIme, $ime, $avatar) {
    $secretKey = getenv('SECRET_KEY');
    $now = time();
    $future = time() + (2 * 60 * 60);

    $token = array(
        'iat' => $now,
        'exp' => $future,
        'korIme' => $korIme,
        'ime' => $ime,
        'avatar' => $avatar
    );

    return JWT::encode($token, $secretKey);
} 